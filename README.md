# Expert systems #

Solutions to the Expert systems (BI-ZNS) homeworks.

* [Probability logic system](pls)
  * You can find there solutions to the **homeworks 1-3**
  * Please read [README.md](pls/README.md) particularly for this set of homeworks
* [Fuzzy logic system](fls)
  * You can find there solution to the **homework 4**
  * Please read [README.md](fls/README.md) particularly for this homework

You can find the progress, during solving the homeworks, in the commit history of this repository. 
