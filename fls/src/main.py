#!/usr/bin/env python3
# |**********************************************************************;
# * Project           : bi-zns-2018-patrotom/fls
# *
# * Program name      : main.py
# *
# * Author            : Tomáš Patro; FIT ČVUT
# *
# * Purpose           : Main module of the project implementing expert
#                       system capable of determing in which climatic
#                       zone the current user is in.
# *
# |**********************************************************************;

import json
import operator

from knowledge_base import Knowledgebase
from config import config


class Defuzzifier:
    '''Contains methods for the defuzzification'''
    def def_centroid(self, fuzzy_set, conclusions, step):
        '''Defuzzificate the results using centroid method'''
        denominator = 0
        numerator = 0
        for k, v in conclusions.items():
            if fuzzy_set[k] != 0:
                for y in range(v[0], v[3] + 1, step):
                    denominator += fuzzy_set[k]
                    numerator += (fuzzy_set[k] * y)
        return (numerator/denominator)


class Fuzzifier:
    '''Contains methods for the fuzzification of the input'''
    def msf_full_trapezoid(self, x, a, b, c, d):
        '''Computes MSF using full trapezoid'''
        return max(min([(x-a)/(b-a), 1, (d-x)/(d-c)]), 0)

    def msf_left_trapezoid(self, x, a, b):
        '''Computes MSF using left trapezoid'''
        if x <= a:
            return 0
        elif x >= b:
            return 1
        else:
            return (x-a)/(b-a)

    def msf_right_trapezoid(self, x, c, d):
        '''Computes MSF using right trapezoid'''
        if x <= c:
            return 1
        elif x >= d:
            return 0
        else:
            return (x-d)/(c-d)
    
    def msf_trapezoid(self, x, p, r):
        '''Decides which trapezoid function will be used based on the input'''
        if (p[0] == r[0]) and (p[1] == r[0]):
            return self.msf_right_trapezoid(x, p[2], p[3])
        elif (p[2] == r[1]) and (p[3] == r[1]):
            return self.msf_left_trapezoid(x, p[0], p[1])
        return self.msf_full_trapezoid(x, p[0], p[1], p[2], p[3])

    def fuzzificate(self, terms, answers):
        '''Fuzzificate the input using trapezoid functions'''
        fuzzy = {}

        for k1, v1 in terms.items():
            fuzzy[k1] = {}
            for k2, v2 in v1.items():
                fuzzy[k1][k2] = self.msf_trapezoid(answers[k1], v2, config['kb']['rating_range'])

        return fuzzy


class InferenceEngine:
    '''
    Contains methods for the correct inference. Better said,
    methods for the gathering input data from the user
    and calling fuzzification on this input
    and defuzzification on the results.
    '''
    def __init__(self):
        self.kb = Knowledgebase()
        self.fuzzi = Fuzzifier()
        self.defuzz = Defuzzifier()

    def get_response(self, term):
        '''Returns response from the user on the particular question'''
        term = term.replace(' ', '')
        x = config['kb']['rating_range'][0]
        y = config['kb']['rating_range'][1]
        print('How would you rate "' + term + '" on a scale [' + str(x)  + ',' + str(y) + ']?\n')

        while True:
            try:
                answer = float(input('Enter the number of points: '))
                if (answer < 0.0) or (answer > 10.0):
                    raise ValueError
            except ValueError:
                print('\nInvalid answer!\n')
            else:
                break
        
        print('\n====================================================\n')

        return answer
    
    def aplicate_rules(self, fuzzified_response, rules):
        '''Applicate rules on the input gathered from the user'''
        fuzzy_set = {}
        for k, v in rules.items():
            t1 = []
            for lst in v:
                t2 = []
                for item in lst:
                    par = item.partition('-')
                    pref = par[0]
                    suff = par[2]
                    t2.append(fuzzified_response[pref][suff])
                t1.append(min(t2))
            fuzzy_set[k] = max(t1)
        return fuzzy_set

    def start(self):
        '''Main handler function controlling whole process'''
        terms = self.kb.load_json(config['kb']['terms'])
        rules = self.kb.parse_rules(config['kb']['rules'])
        conclusions = self.kb.load_json(config['kb']['conclusions']['path'])
        answers = {}
        
        for t in terms:
            answers[t] = self.get_response(t)

        fuzzified_response = self.fuzzi.fuzzificate(terms, answers)
        fuzzy_set = self.aplicate_rules(fuzzified_response, rules)

        d = self.defuzz.def_centroid(fuzzy_set, conclusions, config['kb']['conclusions']['step'])
        print('The tip should be ' + str(d) + ' %')


def main():
    ie = InferenceEngine()
    ie.start()


if __name__ == '__main__':
    main()
