# |**********************************************************************;
# * Project           : bi-zns-2018-patrotom
# *
# * Program name      : knowledge_base.py
# *
# * Author            : Tomáš Patro; FIT ČVUT
# *
# * Purpose           : Module for handling operations over knowledge
#                       base.
# *
# |**********************************************************************;

import os
import json
from collections import OrderedDict


class KnowledgeBaseFileNotFound(Exception):
    pass


class Knowledgebase:
    '''Contains methods for loading and parsing knowledge base data'''
    def parse_rules(self, file):
        '''Parses rules from the file and returns them in the proper data structure'''
        self.file_exists(file)

        with open(file, 'r') as f:
            data = f.readlines()
        data = [l.strip() for l in data]

        out = {}

        for line in data:
            l1 = line.replace('IF ', '').partition(' THEN ')
            conclusion = l1[2]
            l2 = l1[0].split(' OR ')
            out[conclusion] = []
            out[conclusion] = [t.replace('(', '').replace(')', '').split(' AND ') for t in l2]
        
        return out
    
    def file_exists(self, file):
        '''Checks whether a given file exists'''
        if not os.path.isfile(file):
            raise KnowledgeBaseFileNotFound('Knowledge base file \'' + file + '\' does not exist')

    def load_json(self, file):
        '''Loads data from the given json file path'''
        self.file_exists(file)
        with open(file, 'r') as f:
            data = json.load(f, object_pairs_hook=OrderedDict)
        return data
