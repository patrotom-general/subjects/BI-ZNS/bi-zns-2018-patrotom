# Fuzzy logic system #

## Overview ##

This expert system is capable of helping people to calculate percentage ratio of the waiters tip based on the points 
given by the user.

This system solves **Homework 4**.

## Contents ##

1. [Configuration](config/fls.conf)
2. [Knowledge base](knowledge_base/)
   1.  [Conclusions and their membership functions](knowledge_base/conclusions.json)
       * These are the tip percentage categories MSF
   2.  [Terms and their membership functions](knowledge_base/terms.json)
       * These are the properties which user is rating and their MSFs
   3. [Rules](knowledge_base/rules.txt)
3. [Source files](src)
4. [Tests](test)
   * Better said scenarios presented during a homework control
5. [Graphical representation](jupyter/fuzzy_graphical.ipynb)

## Running the system ##

* The system is written in the `Python 3`
* It should be compatible with all versions of Python 3 but it is recommended to run it at least with the version `3.5`
* It is not necessary to use any external modules
    * Used modules should be part of Python's standard library

* [main.py](src/main.py) contains shebang with `Python 3` virtual environment executable so you should be able to run the system without writing "python" before the name of the script
    * Script will automatically decets activated virtual environment executable or if you had not activated the virtual environment, it will use system Python 3

**Usage:**

* Change directory to [src](src/) and run:

``` bash
./main.py
```

or

``` bash
python3 main.py
```

## How it works ##

* System is asking questions based on the terms defined in the [terms.json](knowledge_base/terms.json)
* User is giving the points rating in the given interval which is set in the [fls.confg](config/fls.conf)
* System computes membership functions of the given input, fuzzificate the input and then deffuzificate the result using centroid method
* System is using trapezoids as the membership functions 

**Example:**

``` bash
How would you rate "staff_empathy" on a scale [0,10]?

Enter the number of points: 6

====================================================

How would you rate "staff_reliability" on a scale [0,10]?

Enter the number of points: 6

====================================================

How would you rate "food_quality" on a scale [0,10]?

Enter the number of points: 6.9

====================================================

The tip should be 23.954545454545464 %
```

## Graphical representation ##

Graphical representation is written in the [Jupyter notebook](jupyter/fuzzy_graphical.ipynb) using `Python 3.5` and proper modules. Knowledge base defined in the notebook is same as the one which can be found in the knowledge base of this repository.

You will also find there graphical represenation of defuzzificated results using the same centroid method as in this repository.

You will have to install proper Python 3 modules using pip.

**Usage:**

``` bash
pip3 install numpy jupyter skfuzzy matplotlib
```

### Running Jupyter notebook ###

Navigate yourself to the [jupyter directory](jupyter) and after installing proper Python 3 modules, run `jupyter notebook` command directly from the directory. It will open a browser window for you and you will be able to open particular notebook by clicking on [it](jupyter/fuzzy_graphical.ipynb) in the project browser.
