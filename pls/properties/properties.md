# Car properties #

1. cargo_volume
    * cargo_volume-big - x > 550 l
    * cargo_volume-medium_big - 330 l < x <= 550 l
    * cargo_volume-medium_small - 251 l < x <= 330 l 
    * cargo_volume-small - x <= 251 l
2. vehicle_age
    * vehicle_age-new - new car
    * vehicle_age-old - used car
3. horse_power
    * horse_power-big - x > 120 hp
    * horse_power-medium - 60 < x <= 120 hp
    * horse_power-small - x <= 60 hp
4.  equipment
    * equipment-low
    * equipment-medium
    * equipment-high
5.  offroad
    * offroad-no
    * offroad-yes
6. type
    * type-hatchback
    * type-sedan
    * type-combi
    * type-suv
    * type-mini
