# Probability logic system #

## Overview ##

This expert system is capable of helping people to choose the particular model of a car from the brand [ŠKODA AUTO a.s](http://www.skoda-auto.cz/) based on their preferences, more precisely their input.

This system solves **homeworks 1-3**.

### Homework 1 ###

**Homework 1** is solved by defining the rules in the knowledge base.

### Homework 2 ###

**Homework 2** is solved by programming proper inference mechanism and support for the questions WHY and HOW.

### Homework 3 ###

**Homework 3** is building on the **Homework 2** and adds probabilities and naive Bayes computation.

## Contents ##

1. [Configuration](config/system.conf)
2. [Knowledge base](knowledge_base/)
    1. [Conclusion probabilities](knowledge_base/conclusion_probabilities.txt)
       * These are the conclusions rated by the probabilities
       * Probabilities are based on the popularity of the particular car models
    2. [Conditional probabilities](knowledge_base/conditional_probabilities.txt)
       * These are the probabilities of the particular car properties based on the car models
         * These properties are parts of the rules leading to some particular conclusion (car model)
    3. [Questions and answers](knowledge_base/questions.json)
        * These are the questions the system is asking the user and possible answers to these questions
        * By asking, the system is gathering required user input data
    4. [Rules](knowledge_base/rules.txt) - `Homework 1`
        * IF-THEN rules
3. [List of car models](properties/models.md)
4. [List of watched properties](properties/properties.md)
5. [Source files](src) - `Homework 2 and Homework 3`
6. [Tests](test)
   * Better said scenarios presented during a homework control

## Running the system ##

* The system is written in the `Python 3`
* It should be compatible with all versions of Python 3 but it is recommended to run it at least with the version `3.5`
* It is not necessary to use any external modules
    * Used modules should be part of Python's standard library

* [main.py](src/main.py) contains shebang with `Python 3` virtual environment executable so you should be able to run the system without writing "python" before the name of the script
    * Script will automatically decets activated virtual environment executable or if you had not activated the virtual environment, it will use system Python 3

**Usage:**

* Change directory to [src](src/) and run:

``` bash
./main.py
```

or

``` bash
python3 main.py
```

## How it works ##

* System will be asking the user set of questions which are defined in the [questions.json](knowledge_base/questions.json) file
* Questions are based on the watched properties (conditions) of the cars
    * The system will check available properties in the [rules.txt](knowledge_base/rules.txt) and will give the user only questions based on them
* System will offer the user set of possible answers (also defined in the [questions.json](knowledge_base/questions.json) file) and user will have to pick one of them
    * Answers are always numbered and user has to pass one of the possible numbers of answers as the input for the system
    * If the user passes invalid input, the system will ask the user to answer again
* User is able to ask `WHY` question after each answer
    * System will tell the user what it is trying to proof
* User is also able to ask `HOW` question once the system outputs a conclusion
    * System will show the user his/her whole path of questions and answers leading to the particular conclusion

## User input ##

* System will be asking questions and will give you the set of answers with numbers next to them
* Type number of the answer, space and a probability of your answer
    * Probability should be a float number in the range <0.0,1.0>
* Instead of number and probability, you can ask the system why it is giving you some particular question
    * Simply by writing down `why` as stdin
    * You will be able to answer the question afterwards

**Example:**

```
How large cargo volume do you want?

        0 : more than 550 litres
        1 : from 331 litres to 550 litres
        2 : from 252 litres to 330 litres
        3 : less than or equal to 251 litres

Enter the number of answer: 1 0.78
====================================================
Do you want a new car or a used car?

        0 : new car
        1 : used car

Enter the number of answer: why

System is trying to prove: 'conclusion' = 'rapid_spaceback'

----------------------------------------------------

Enter the number of answer: 0 0.95
====================================================
How many horses do you want your car to have?

        0 : more than 120 horses
        1 : from 61 horses to 120 horses
        2 : less than or equal to 60 horses

Enter the number of answer:
```
