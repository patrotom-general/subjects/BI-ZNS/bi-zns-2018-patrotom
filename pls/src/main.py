#!/usr/bin/env python3
# |**********************************************************************;
# * Project           : bi-zns-2018-patrotom
# *
# * Program name      : main.py
# *
# * Author            : Tomáš Patro; FIT ČVUT
# *
# * Purpose           : Main module of the project implementing expert
#                       system capable of recommending a suitable type
#                       of the ŠKODA AUTO a.s car.
# *
# |**********************************************************************;

import json
import copy
import os
from decimal import Decimal, getcontext
from collections import OrderedDict
from operator import itemgetter

from config import config
from knowledge_base import KnowledgeBase, KnowledgeBaseFileNotFound


class ConclusionCanNotBeProved(Exception):
    pass


class InferenceEngine:
    '''Handles whole process of asking, evaluating and explaining'''
    def __init__(self):
        self.kb = KnowledgeBase()
        self.asked_predicates = []
        self.conclusions = []
        self.user_path = []

    def find_predicate(self):
        '''Find a predicate on which a user will be answering'''
        for car, properties in self.kb.data.items():
            for p in properties:
                if p.partition('-')[0] not in self.asked_predicates:
                    return car, p.partition('-')[0]
        
        raise ConclusionCanNotBeProved()

    def prove_conclusion(self):
        '''Checks whether the current base of facts leads to some conclusion'''
        for car, properties in self.kb.data.items():
            if set(properties) == set(self.kb.facts.keys()):
                self.conclusions.append(car)
        if len(self.conclusions) != 0:
            return True
        return False
    
    def squeeze_selection(self, answer):
        '''Eliminates conclusions that can not be answered'''
        to_remove = []
        for car, properties in self.kb.data.items():
            if answer not in properties:
                to_remove.append(car)
        
        for r in to_remove:
            self.kb.data.pop(r, None)

    def compute_bayes(self):
        '''Computes naive bayes based on the final conclusion'''
        results = {}
        rule_prob = {}
        getcontext().prec = 4
        
        for c in self.conclusions:
            tmp = Decimal(1.0)
            for k, v in self.kb.facts.items():
                tmp *= Decimal(self.kb.cond_probs[c][k]) * Decimal(v)
            tmp *= Decimal(self.kb.conc_probs[c])
            rule_prob[c] = tmp
        
        s = Decimal(sum(rule_prob.values()))
        for k, v in rule_prob.items():
            results[k] = v / s
        return {i: results[i]/sum(results.values()) * 100 for i in results}

    def explain_why(self, conclusion):
        '''
        Handles user's potential questions on why the system is giving
        him/her questions like these.
        '''
        print('\nSystem is trying to prove: \'conclusion\' = \'' + conclusion + '\'\n')
        print('----------------------------------------------------')
    
    def explain_how(self, user_path):
        '''
        Handles user's potential questions on how the system came to
        the particular conclusion. User will be capable of asking
        HOW once the system will give him/her the conclusion.
        '''

        print('\nDo you want to know how the system came to this conclusion?\n\n\t 0 : no\n\t 1 : yes\n')

        while True:
            try:
                option = input('Enter the number of answer: ')
                if int(option) not in range(0, 2):
                    raise ValueError
            except ValueError:
                print('\nInvalid answer!\n')
            else:
                break

        if int(option) == 1:
            print('\nThe system came to this conclusion by following this path:\n')
            for i in range(0, len(user_path)):
                print('\t' + user_path[i][0] + ' -> ' + user_path[i][1])

    def return_answer(self, predicate):
        '''
        Asks the user a question and affords him/her possible answers.
        Returns the answer of the user.
        '''
        answers = []
        print(self.kb.questions[predicate[1]]['message'] + '\n')
        cnt = 0
        for a in self.kb.questions[predicate[1]]['answers']:
            print('\t' + str(cnt) + ' : ' + self.kb.questions[predicate[1]]['answers'][a])
            answers.append(a)
            cnt += 1

        while True:
            try:
                ans = input('\nEnter the number of answer: ').split()
                
                if len(ans) == 1 and ans[0].lower() == 'why':
                    self.explain_why(predicate[0])
                elif len(ans) == 1:
                    raise ValueError
                elif (int(ans[0]) not in range(0, len(answers))) or float(ans[1]) > 1.0 or float(ans[1]) < 0.0 or len(ans) > 2:
                    raise ValueError
                else:
                    num = int(ans[0])
                    prob = float(ans[1])
                    break
            except ValueError:
                print('\nInvalid answer!')
        
        print('====================================================')

        self.kb.facts[predicate[1] + '-' + answers[num]] = prob
        self.asked_predicates.append(predicate[1])

        return predicate[1] + '-' + answers[num]
    
    def show_results(self):
        '''Shows final conclusions rated by its probabilities'''
        print('System came to this conclusion/these conclusions with this probability/these probabilities:\n')
        results = OrderedDict(sorted(self.compute_bayes().items(), key=itemgetter(1), reverse=True))

        cnt = 0
        hidden = False
        for k, v in results.items():
            if cnt == config['conclusion_limit']:
                hidden = True
                break
            print(k, '-', v, '%')
            cnt += 1
        if hidden:
            print('\nNot all records were listed, do you want to list all of them?\n\n\t 0 : no\n\t 1 : yes\n')
            while True:
                try:
                    option = input('Enter the number of answer: ')
                    if int(option) not in range(0, 2):
                        raise ValueError
                except ValueError:
                    print('\nInvalid answer!\n')
                else:
                    break
            if int(option) == 1:
                print()
                for k, v in results.items():
                    print(k, '-', v, '%')
        self.explain_how(self.user_path)

    def start(self):
        '''
        Main method which will start whole process of asking the questions
        and evaluating the rules. User is also cabale of asking the
        questions how and why.
        '''
        self.kb.load_all()
        while True:
            try:
                predicate = self.find_predicate()
                answer = self.return_answer(predicate)
                self.squeeze_selection(answer)
                
                self.user_path.append(tuple((self.kb.questions[predicate[1]]['message'], self.kb.questions[predicate[1]]['answers'][answer.partition('-')[2]])))
                
                if self.prove_conclusion():
                    self.show_results()
                    break
            except ConclusionCanNotBeProved:
                print('Conclusion can not be proved because following path is not leading to any conclusion at all: \n')
                for i in range(0, len(self.user_path)):
                    print('\t' + self.user_path[i][0] + ' -> ' + self.user_path[i][1])
                break


def main():
    h = InferenceEngine()
    h.start()


if __name__ == '__main__':
    main()
