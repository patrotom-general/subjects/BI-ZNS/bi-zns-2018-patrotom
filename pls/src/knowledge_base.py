# |**********************************************************************;
# * Project           : bi-zns-2018-patrotom
# *
# * Program name      : knowledge_base.py
# *
# * Author            : Tomáš Patro; FIT ČVUT
# *
# * Purpose           : Module for handling operations over knowledge
#                       base.
# *
# |**********************************************************************;
import os
import json
from collections import OrderedDict

from config import config


class KnowledgeBaseFileNotFound(Exception):
    pass


class KnowledgeBase:
    '''Operations over the knowledge base'''
    def __init__(self):
        self.data = {}
        self.facts = {}
        self.questions = {}
        self.conc_probs = {}
        self.cond_probs = {}
    
    def parse_lines(self, file):
        '''Parse lines from the file and adds it into the list'''
        if not os.path.isfile(file):
            raise KnowledgeBaseFileNotFound('Knowledge base file \'' + file + '\' does not exist')

        with open(file, 'r') as f:
            data = f.readlines()
        data = [l.strip() for l in data]
        return data
    
    def load_all(self):
        '''Loads all necesary files'''
        self.load_rules()
        self.load_questions()
        self.load_conc_probs()
        self.load_cond_probs()

    def load_rules(self):
        '''
        Loads file with the knowledge base configured in the config file.
        Path to the file is configured in the config file located in the
        /config directory.Knowledge base contains rules. This method will
        parse those rules into Python data structure.
        '''
        data = self.parse_lines(config['knowledge_base']['rules'])

        for line in data:
            conclusion = line.partition('THEN')[2].replace(' ', '')
            predicates = line.partition('IF')[2].partition('THEN')[0]
            predicates = predicates.split(' AND ')

            self.data[conclusion] = []
            for predicate in predicates:
                predicate = predicate.replace(' ', '')
                self.data[conclusion].append(predicate)

    def load_questions(self):
        '''
        Loads questions which will be used to gather facts from the user
        Questions are stored in the JSON file format. Path to the file
        is configured in the config file located in the /config directory.
        '''
        if not os.path.isfile(config['knowledge_base']['questions']):
            raise KnowledgeBaseFileNotFound('Questions file \'' + config['knowledge_base']['questions'] + '\' does not exist')
        with open(config['knowledge_base']['questions'], 'r') as f:
            self.questions = json.load(f, object_pairs_hook=OrderedDict)
    
    def load_conc_probs(self):
        '''Loads conclusion probabilities file'''
        data = self.parse_lines(config['knowledge_base']['conclusion_probabilities'])
        for line in data:
            l = line.split()
            self.conc_probs[l[0]] = float(l[1])
    
    def load_cond_probs(self):
        '''Loads conditional probabilities file'''
        data = self.parse_lines(config['knowledge_base']['conditional_probabilities'])
        for line in data:
            l = line.split()
            if l[1] not in self.cond_probs:
                self.cond_probs[l[1]] = {}
            self.cond_probs[l[1]][l[0]] = float(l[2])
