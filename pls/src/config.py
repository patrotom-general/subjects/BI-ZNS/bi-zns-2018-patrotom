# |**********************************************************************;
# * Project           : bi-zns-2018-patrotom
# *
# * Program name      : config.py
# *
# * Author            : Tomáš Patro; FIT ČVUT
# *
# * Purpose           : Module for basic configuration of system.
# *
# |**********************************************************************;

import os
import json

CONFIG_PATH = '../config/system.conf'

global config


class ConfigFileNotFound(Exception):
    pass


if os.path.isfile(CONFIG_PATH):
    with open(CONFIG_PATH, 'r') as f:
        config = json.load(f)
else:
    raise ConfigFileNotFound('Config file \'' + CONFIG_PATH + '\' does not exist')
